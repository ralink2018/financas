package br.com.caelum.financas.teste;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.caelum.financas.modelo.Conta;

public class TesteInsereConta {

	public static void main(String[] args) {
		
		long inicio = System.currentTimeMillis();
		long fim = System.currentTimeMillis();
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("controlefinancas");
		
		EntityManager manager = factory.createEntityManager();
		
		Conta conta = new Conta();
		conta.setTitular("Helena");
		conta.setBanco("Banco do Brasil");
		conta.setNumero("133746-8");
		conta.setAgencia("768");
		
		manager.getTransaction().begin();
		manager.persist(conta);
		manager.getTransaction().commit();
		manager.close();
		factory.close();
		
		System.out.println("Conta Gravada com Sucesso!");
		System.out.println("Executado em: " + (inicio - fim) + "ms");
	}

}
